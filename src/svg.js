const image = "<svg height=\"400\" width=\"450\">\n" +
"<path id=\"lineAB\" d=\"M 100 350 l 150 -300\" stroke=\"red\" stroke-width=\"3\" fill=\"none\" />\n" +
"  <path id=\"lineBC\" d=\"M 250 50 l 150 300\" stroke=\"red\" stroke-width=\"3\" fill=\"none\" />\n" +
"  <path d=\"M 175 200 l 150 0\" stroke=\"green\" stroke-width=\"3\" fill=\"none\" />\n" +
"  <path d=\"M 100 350 q 150 -300 300 0\" stroke=\"blue\" stroke-width=\"5\" fill=\"none\" />\n" +
"  <!-- Mark relevant points -->\n" +
"  <g stroke=\"black\" stroke-width=\"3\" fill=\"black\">\n" +
"    <circle id=\"pointA\" cx=\"100\" cy=\"350\" r=\"3\" />\n" +
"    <circle id=\"pointB\" cx=\"250\" cy=\"50\" r=\"3\" />\n" +
"    <circle id=\"pointC\" cx=\"400\" cy=\"350\" r=\"3\" />\n" +
"  </g>\n" +
"  <!-- Label the points -->\n" +
"  <g font-size=\"30\" font-family=\"sans-serif\" fill=\"black\" stroke=\"none\" text-anchor=\"middle\">\n" +
"    <text x=\"100\" y=\"350\" dx=\"-30\">A</text>\n" +
"    <text x=\"250\" y=\"50\" dy=\"-10\">B</text>\n" +
"    <text x=\"400\" y=\"350\" dx=\"30\">C</text>\n" +
"  </g>\n" +
"  Sorry, your browser does not support inline SVG.\n" +
"</svg>";

export default btoa(image)
