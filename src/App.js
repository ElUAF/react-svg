import logo from './logo.svg';
import './App.css';
import SvgImage from "./SvgImage";

import svgBase64Image from "./svg";

function App() {
  return (
    <div className="App">
      <header className="App-header">

        <SvgImage imageBase64={svgBase64Image} />

        <SvgImage imageBase64={btoa("invalid_svg")} />
      </header>
    </div>
  );
}

export default App;
