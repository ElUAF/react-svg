import React, {useMemo} from "react";
import isSvg from "is-svg";

const SvgImage = ({ imageBase64 }) => {
    const validImage = useMemo(() => {
        const rawImage = atob(imageBase64);

        if (imageBase64 && !isSvg(rawImage)) {
            return null;
        } else {
            return rawImage;
        }
    }, [imageBase64]);

    if (validImage) {
        return <span dangerouslySetInnerHTML={{__html: validImage}}/>
    } else {
        return null;
    }
}

export default SvgImage;
